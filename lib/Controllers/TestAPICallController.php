<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SMLPController;

$utilsDir = dirname(dirname(__FILE__)) . '/LoggerData';
$testTLoggerFile = $utilsDir . '/TestCallLog.txt';

/**
 * Description of TestAPICallController
 *
 * @author omkarkhedekar
 */
class TestAPICallController {

    //put your code here

    public static function log($sendDispatch = FALSE) {
        date_default_timezone_set('UTC');


        $headers = apache_request_headers();
        $fh = fopen($testTLoggerFile, 'a') or die("can't open file");

        $stringData = "================================\n" . "TS:" . date(DATE_RFC2822) . "\n";
        $stringData = $stringData . "Request Type : " . filter_input(INPUT_SERVER, 'REQUEST_METHOD') . "\n";
        foreach ($headers as $key => $value) {

            $stringData = $stringData . "\n" . $key . " : " . $headers[$key] . "\n";
        }
//application/json
        if (isset($headers["Content-Type"]) && $headers["Content-Type"] === "application/json") {
            $json = file_get_contents('php://input');
            $stringData = $stringData . "\n PARAMS JSON : " . $json;
        } else {
            foreach ($_REQUEST as $key => $value) {

                $stringData = $stringData . "\n" . $key . " : " . $_REQUEST[$key] . "\n";
            }
        }

        fwrite($fh, $stringData);
        fclose($fh);

        if (1) {
            
            $error = array();

            $error['status'] = 200;
            $error ['message'] = 'Logged';
            $error ['time'] = date(DATE_RFC2822);
            echo json_encode($error);
        }
    }

}
