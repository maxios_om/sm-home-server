<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SMLPController;

$utilsDir = dirname(dirname(__FILE__)) . '/Models';
include_once $utilsDir . '/SMLPUser.php';


$utilDir = dirname(dirname(__FILE__)) . '/Utils';
include_once $utilDir . '/Crypto/CryptoUtils.php';

use CryptoUtils as CommanCrypto;

/**
 * Description of EncDecTestController
 *
 * @author omkarkhedekar
 */
class EncDecTestController {

    //put your code here

    public static function testFunc() {
        $headers = apache_request_headers();

        $_userID = $headers['x-api-user'];
        $function = $headers['x-api-func'];
        $testUser = new \SMLPUser();
        $userToken = @"@1L0veShr";/*$testUser->getUserTokenWithID($_userID);*/
        $str = NULL;
        $sampleString = "I love you Shraddha I love you sooo much I cannot live with out you So i have decided to die Hope you will forgive me atleast afterwords";

//        $encrypted = "";
//        if ($function == "encrypt") {
            $encrypted = CommanCrypto::encrypt($sampleString, $userToken);
//        } else {
            $str = $headers['x-api-encd'];
            $dencrypted = CommanCrypto::decrypt($str, $userToken);
//        }
//        $d = CommanCrypto::decrypt($encrypted, $password)
        $dispatchData = array();
        $dispatchData['encrypted'] = $encrypted;
        $dispatchData['dencrypted'] = $dencrypted;
        $dispatchData['received'] = $str;
        $dispatchData['org'] = $sampleString;
        $dispatchData['func'] = $function;
        $dispatchData['time'] = date(DATE_RFC2822);
//        $dispatchData['received'] = 
            
        EncDecTestController::dispatch($dispatchData);
        
    }
    
    public static function dispatch($param) {
        /*
         *  $error = array();

            $error['status'] = 200;
            $error ['message'] = 'Logged';
            $error ['time'] = date(DATE_RFC2822);
            echo json_encode($error);
         */
        $usr = new \SMLPUser();
        $usr->sentResponse(200, $param);
        return;
    }
    

}

EncDecTestController::testFunc();

