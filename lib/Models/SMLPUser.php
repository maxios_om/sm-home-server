<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("AbstractModel.php");

$utilsDir = dirname(dirname(__FILE__)) . '/Utils';
include_once $utilsDir . '/Crypto/CryptoUtils.php';

use CryptoUtils as CommanCrypto;

/*
  user_master	"CREATE TABLE `user_master` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(128) NOT NULL,
  `user_pass` varchar(128) NOT NULL,
  `nick_name` varchar(64) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `pass_salt` varchar(128) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  UNIQUE KEY `pass_salt` (`pass_salt`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf32"

 * 
 * CREATE TRIGGER On_New_User BEFORE INSERT ON user_master 
  FOR EACH ROW
  SET NEW.created_on = NOW();

 */

/**
 * Description of SMLPUser
 *
 * @author omkarkhedekar
 */
class SMLPUser extends \Models\AbstractModel {

    //Public properties 
    public $user_id;
    public $user_email;
    public $nick_name;
    public $created_on;
    public $is_active;
    private $user_pass;
    private $pass_salt;

    // overides 


    protected function getVisiblecolumnList() {
        parent::getVisiblecolumnList();
        return array('user_id', 'user_email', 'nick_name', 'created_on', 'is_active');
    }

    private function modelFromDBResult($DBResultSet) {


        $user = new SMLPUser();
        $user->user_id = $DBResultSet['user_id'];
        $user->user_email = $DBResultSet['user_email'];
        $user->nick_name = $DBResultSet['nick_name'];
        $user->user_pass = $DBResultSet['user_pass'];
        $user->pass_salt = $DBResultSet['pass_salt'];
        $user->is_active = $DBResultSet['is_active'];
        $user->created_on = $DBResultSet['created_on'];

        return $user;
    }

//        private 
//        private addUser


    private function register() {

        $data = Array();
        $data['user_email'] = $this->user_email;
        $data['nick_name'] = $this->nick_name;
        $data['user_pass'] = $this->user_pass;
        $data['pass_salt'] = $this->pass_salt;

        $id = $this->_DB->insert('user_master', $data);
        if ($id) {

            $resp = array();
            $resp ['user_id'] = $id;
            $resp ['message'] = 'user was created.';
            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();

            $this->sentResponse(406, $resp);
        }
    }

    private function getUser() {

        $onCol = '*';
        $val = '';
        if (isset($this->_requestData['user_id'])) {
            $onCol = 'user_id';
            $val = $this->_requestData['user_id'];
        }

        if (isset($this->_requestData['user_email'])) {
            $onCol = 'user_email';
            $val = $this->_requestData['user_email'];
        }

        $this->_DB->where($onCol, $val);
        $results = $this->_DB->getOne($this->tableName(), $this->getVisiblecolumnList());
        if (empty($results)) {

            $this->sentResponse(204);
        } else {
            $this->sentResponse(200, $results);
        }
    }

    private function msTimeStamp() {
        return round(microtime(1) /** 1000 */);
    }

    private function createUserTokenIfNeeded() {

        $now = $this->msTimeStamp();
        $b410Days = $now - 86400000;
        $this->_DB->where('user_id', $this->user_id);
        $this->_DB->where("isValid", 1);
        $this->_DB->where('created_on', Array($b410Days, $now), 'BETWEEN');
        $results = $this->_DB->get('user_session');
        if (!empty($results)) {

            return $results[0]['session_key'];
        } else {
            //create new one 
            $data = Array();
            $data['user_id'] = $this->user_id;
            $data['session_key'] = CryptoUtils::UniqueSalt();
            $data['user-agent'] = $this->_RequestHeaders["User-Agent"];
            $data['isValid'] = 1;
            //invalidate old sesions 
            $invalidateOthers = array();
            $invalidateOthers['isValid'] = 0;
            $this->_DB->where('user_id', $this->user_id);
            $this->_DB->update('user_session', $invalidateOthers);

            $id = $this->_DB->insert('user_session', $data);
            if ($id) {

                return $data['session_key'];
            } else {
                echo $this->_DB->lastError();
                exit();
                return FALSE;
            }
        }

        return FALSE;
    }

    private function validateLogin($userName, $password) {

        if (empty($userName) || empty($password)) {

            $this->sentResponse(400, array("Message" => "Incomplete Request user name or password meassing"));
        } else {
            //is_active
            $this->_DB->where("is_active", 1);
            $this->_DB->where("user_email", $userName);
            $userRS = $this->_DB->getOne("user_master");
            $userPassSalt = $userRS['pass_salt'];
            $userModel = $this->modelFromDBResult($userRS);


            if (password_verify($password, $userPassSalt)) {

                $user_token = $userModel->createUserTokenIfNeeded();
                unset($userRS['pass_salt']);
                unset($userRS['user_pass']);
                if ($user_token != FALSE) {
                    //user_pass
                    $userRS['user_token'] = $user_token;

                    $this->sentResponse(200, $userRS, "user");
                } else {
                    $this->sentResponse(204, array('Message' => 'Invalid user.'));
                }
//                echo 'Password is valid!';
            } else {
                $this->sentResponse(204, array('Message' => 'Invalid password or user name.'));
//                echo 'Invalid password.';
            }
        }
    }

    public function doLogin($userName, $password) {

        if (!$this->validateLogin($userName, $password)) {
            throw new Exception("Invalid Login Credentials");
        }
        return TRUE;
    }

    public function doLogout() {



        return 'logout!';
    }

    public function addNewUser() {

        $user = new SMLPUser();
        $user->user_email = $this->_requestData['user_email'];
        $user->nick_name = $this->_requestData['nick_name'];
        $user->user_pass = $this->_requestData['user_pass'];
        $user->pass_salt = password_hash($user->user_pass, PASSWORD_DEFAULT);
        $user->register();
    }

    public function doUpdate() {

//        var_dump($this->_requestData);
    }

    public function listAllUsers() {

//        var_dump($this->_requestData);


        $cols = $this->getVisiblecolumnList();

        $users = $this->_DB->get("user_master", null, $cols);
        if ($users) {
            $this->sentResponse(200, $users, 'users');
        } else {
            $this->sentResponse(204);
        }
    }

    public function hello() {
        var_dump($this->_requestData);
        $user_id = $this->_requestData['id'];

        $resp = array();
        $resp['Message'] = 'Hello ' . $user_id;
        $this->sentResponse(200, $resp);
    }

    public function getUserTokenWithID($_userID) {
        $this->_DB->where('user_id', $_userID);
        $this->_DB->where("isValid", 1);
        $results = $this->_DB->get('user_session');
        $last = end($results);
        return $last['session_key'];
    }

    public function handleQuery() {
        parent::handleQuery();

        switch ($this->_RequestType) {
            case "POST": {
                    switch ($this->_RequestPath) {
                        case "login":

                            $this->validateLogin($this->_requestData['user_email'], $this->_requestData['user_pass']);
                            break;

                        case "register":

                            $this->addNewUser();
                            break;

                        default:

                            $this->sentResponse(406, $error);
                            break;
                    }
                }
                break;
            case "GET":
                switch ($this->_RequestPath) {
                    case "users":

                        $this->listAllUsers();
                        break;
                    case "user":

                        $this->getUser($this->_requestData['user_id']);
                        break;
                    default:

                        break;
                }

                break;

            case "PUT":

                $this->showStatus("Chindya PUT");
                break;

            case "DELETE":

                $this->showStatus("Chindya DELETE");
                break;
            default:

                $this->sentResponse(404);

                break;
        }
    }

}

$SMLPUSER = new SMLPUser();

$SMLPUSER->handleQuery();
