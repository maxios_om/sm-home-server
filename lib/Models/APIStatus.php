<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Models;
require_once("AbstractModel.php");
//require '\Models\AbstractModel';
/**
 * Description of APIStatus
 *
 * @author omkarkhedekar
 */
class APIStatus extends AbstractModel{
    //put your code here
    
    
    public function showStatus($status) {
        
//        $this-sentResponse
        $message = array();
//        $message['Message'] = $status;
        $this->sentResponse(200);
        
//        return $this->jsonResponse();
    }
    
    
    public function handleQuery() {
        parent::handleQuery();
        
        switch ($this->_RequestType) {
            case "POST":

                $this->showStatus("Chindya Post");
                break;
            
            case "GET":

                $this->showStatus("Chindya Get");
                break;
            
            case "PUT":

                $this->showStatus("Chindya PUT");
                break;
            
            case "DELETE":

                $this->showStatus("Chindya DELETE");
                break;
            default:
                break;
        }
    }
}


$currentAPIStatus = new APIStatus ();

$currentAPIStatus->handleQuery();