<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

require_once("AbstractModel.php");
/**
 * Description of SensorData
 *
 * @author omkarkhedekar
 */

/**
  CREATE TABLE `sensor_data` (
  `sensor_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `data` int(11) DEFAULT NULL,
  `data_type` varchar(32) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sensor_data_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf32;

  INSERT INTO `iotdb`.`sensor_data`
  (`sensor_data_id`,
  `sensor_id`,
  `state`,
  `data`,
  `data_type`,
  `created_on`,
  `updated_on`)
  VALUES
  (<{sensor_data_id: }>,
  <{sensor_id: }>,
  <{state: 1}>,
  <{data: }>,
  <{data_type: }>,
  <{created_on: }>,
  <{updated_on: CURRENT_TIMESTAMP}>);

 */
class SensorData extends AbstractModel {

    //put your code here
    // sensor state
    const ON = 1;
    const OFF = 0;
    /*
     * sensor data types 
     * E.g. 
     * temp:c = temprature celsius
     * temp:f = temprature fahrenheit
     */
    const temprature_celsius = 'temp:c';
    const temprature_fahrenheit = 'temp:f';

    private $sensor_data_id;
    public $sensor_id;
    public $state;
    public $data;
    public $data_type;
    public $created_on;
    public $updated_on;

    protected function getVisiblecolumnList() {
        parent::getVisiblecolumnList();
        return array('sensor_id', 'state', 'data', 'data_type', 'created_on'/* ,'updated_on' */);
    }

    protected function getCompactColumnList() {
        parent::getVisiblecolumnList();
        return array('data', 'data_type', 'created_on', /* 'updated_on' */);
    }

    protected function tableName() {
        parent::tableName();
        return 'sensor_data';
    }

    public function getRecordsForSensor($_sensorID, $from = NULL, $to = NULL, $COMPACT = TRUE, $JSON = TRUE) {

        $this->_DB->where('sensor_id', $_sensorID);

        if ($from != NULL && $to != NULL) {
            $this->_DB->where('created_on', Array($from, $to), 'BETWEEN');
        } elseif ($from != NULL) {
            $this->_DB->where('created_on', $from, ">=");
        } elseif ($to != NULL) {
            $this->_DB->where('created_on', $to, "<=");
        }

        $coloums = NULL;

        if ($COMPACT) {

            $coloums = $this->getCompactColumnList();
        } else {
            $coloums = $this->getVisiblecolumnList();
        }
        $results = $this->_DB->get($this->tableName(), NULL, $coloums);

        if (empty($results)) {
            if ($JSON) {
                $this->sentResponse(204);
            } else {
                return $results;
            }
        } else {
            if ($JSON) {
                $this->sentResponse(200, $results, 'sensor_data');
            } else {
                return $results;
            }
        }
    }

    public function store() {

        $data = Array();
        $data['sensor_id'] = $this->sensor_id;
        $data['state'] = $this->state;
        $data['data'] = $this->data;
        $data['data_type'] = $this->data_type;

        $id = $this->_DB->insert($this->tableName(), $data);


        if ($id) {
            $resp = array();

            $resp ['message'] = 'Record Added.';

            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();
            $resp ['params'] = $this->_requestData;
            $this->sentResponse(406, $resp);
        }
    }

    public function storeForDevice($_device_id) {

        $data = Array();
        $data['sensor_id'] = $this->sensor_id;
        $data['state'] = $this->state;
        $data['data'] = $this->data;
        $data['data_type'] = $this->data_type;

        $id = $this->_DB->insert($this->tableName(), $data);


        if ($id) {
            $resp = array();


            $resp ['message'] = 'Record Added.';

            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();
            $resp ['params'] = $this->_requestData;
            $this->sentResponse(406, $resp);
        }
    }

    private function handleGet() {

        $this->sentResponse(200);
    }

    protected function handlePost() {
        switch ($this->_RequestPath) {

            case "record":

                $this->record();
                break;

//            case "update":
//
//                $this->updateRec();
//                break;

            default:
                $this->sentResponse(404);
                break;
        }
    }

    private function record() {

        $device = new SensorData();
        $device->sensor_id = $this->_requestData['sensor_id'];
        $device->state = $this->_requestData['sensor_state'];
        $device->data = $this->_requestData['sensor_data'];
        $device->data_type = $this->_requestData['sensor_data_type'];
        $device->addNewRecord($device);
    }

    public function handleQuery() {
        parent::handleQuery();

        switch ($this->_RequestType) {
            case "POST":
                $this->handlePost();
                break;
            case "GET":

                $this->handleGet();
                /*
                  switch ($this->_RequestPath) {
                  case "devices":

                  $this->listAllUsers();
                  break;

                  default:
                  $this->getUser();
                  break;
                  }
                 */

//                $this->showStatus("Chindya GET");
                break;

            case "PUT":

//                $this->showStatus("Chindya PUT");
                break;

            case "DELETE":

//                $this->showStatus("Chindya DELETE");
                break;
            default:

                $this->sentResponse(404);

                break;
        }
    }

}
