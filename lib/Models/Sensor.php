<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

require_once("AbstractModel.php");
require_once("SensorData.php");

/**
 * Description of Sensor
 *
 * @author omkarkhedekar
 */
/*
  CREATE TABLE `sensor` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `sensor_type` int(11) NOT NULL DEFAULT '1',
  `sensor_name` varchar(45) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sensor_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf32;

 * 
 * INSERT INTO `iotdb`.`sensor`
  (`sersor_id`,
  `device_id`,
  `sensor_type`,
  `sensor_name`,
  `is_active`,
  `created_on`,
  `updated_on`)
  VALUES
  (<{sersor_id: }>,
  <{device_id: }>,
  <{sensor_type: 1}>,
  <{sensor_name: }>,
  <{is_active: 1}>,
  <{created_on: }>,
  <{updated_on: CURRENT_TIMESTAMP}>);


 */

class Sensor extends AbstractModel {

    //put your code here
    public $sensor_id;
    public $device_id;
//    public $sensor_type;
    public $sensor_name;
    public $is_active;
    public $created_on;
    public $updated_on;

    protected function getVisiblecolumnList() {
        parent::getVisiblecolumnList();

        return array('sensor_id', 'sensor_name', 'location','is_active', 'created_on', 'updated_on');
    }

    protected function tableName() {
        parent::tableName();
        return 'sensor';
    }

    private function addNewSensor(Sensor $device) {

//        var_dump($device);
//        exit();

        $data = Array();
        $data['device_id'] = $device->device_id;
        $data['sensor_name'] = $device->sensor_name;

        $id = $this->_DB->insert($this->tableName(), $data);


        if ($id) {
            $resp = array();
            $resp ['sersor_id'] = $id;
            $resp['sensor_name'] = $device->sensor_name;
            $resp ['message'] = 'Sensor is registered.';

            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();

            $this->sentResponse(406, $resp);
        }
    }

    private function register() {
        $device = new Sensor();
        $device->device_id = $this->_requestData['device_id'];
        $device->sensor_name = $this->_requestData['sensor_name'];

        $device->addNewSensor($device);
    }

    private function getAddRecord() {
        $device = new SensorData();
        $device->sensor_id = $this->_requestData['sensor_id'];
        $device->state = $this->_requestData['sensor_state'];
        $device->data = $this->_requestData['sensor_data'];
        $device->data_type = $this->_requestData['sensor_data_type'];
        $device->store();
    }

    private function getSensors_internal() {

        if (array_key_exists('device_id', $this->_requestData)) {

            $_deviceID = $this->_requestData['device_id'];
            $this->getSensorsWithDeviceID($_deviceID);
        } elseif (array_key_exists('sensor_id', $this->_requestData)) {

            $_sensor_id = $this->_requestData['sensor_id'];
            $this->getSensorWithID($_sensor_id);
        } else {

            $this->sentResponse(404);
        }
    }

    private function getSensorLog() {
        if (isset($this->_requestData['sensor_id'])) {
            
            $sensor = $this->_requestData['sensor_id'];
            $fromDate = NULL;
            $toDate = NULL;
            $compact = FALSE;

            if (isset($this->_requestData['from'])) {

                $fromDate = $this->_requestData['from'];
            }

            if (isset($this->_requestData['to'])) {

                $toDate = $this->_requestData['to'];
            }
            
            if (isset($this->_requestData['compact'])) {

                $toDate = boolval($this->_requestData['compact']);
            }


            $sensData = new SensorData();
            $sensData->getRecordsForSensor($sensor, $fromDate, $toDate, TRUE);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    protected function handleGet() {

        switch ($this->_RequestPath) {
            case 'data': {

                    $res = $this->getSensorLog();
                    if (!$res) {
                        $this->sentResponse(404);
                    }
                }
                break;
            case 'sensors':
                $this->getSensors_internal();
                break;

            default :
                $this->sentResponse(404);
                break;
        }
    }

//    private 
    protected function handlePost() {
        parent::handlePost();
        switch ($this->_RequestPath) {

            case "register":

                $this->register();
                break;
            case "record":
                $this->getAddRecord();
                break;

            default:
                $this->sentResponse(404);
                break;
        }
    }

    /*

     */

    public function getSensorsWithDeviceID($_deviceID, $JSON = TRUE) {

        $this->_DB->where('device_id', $_deviceID);
        $results = $this->_DB->get($this->tableName(), NULL, $this->getVisiblecolumnList());

        if (empty($results)) {
            if ($JSON) {
                $this->sentResponse(204);
            } else {
                return $results;
            }
        } else {
            if ($JSON) {
                $this->sentResponse(200, $results, 'sensors');
            } else {
                return $results;
            }
        }
    }

    public function getSensorWithID($_sensor_id, $JSON = TRUE) {

        $this->_DB->where('sensor_id', $_sensor_id);
        $results = $this->_DB->getOne($this->tableName(), NULL, $this->getVisiblecolumnList());

        if (empty($results)) {
            if ($JSON) {
                $this->sentResponse(204);
            } else {
                return $results;
            }
        } else {
            if ($JSON) {
                $this->sentResponse(200, $results, 'sensor');
            } else {
                return $results;
            }
        }
    }

    public function handleQuery() {
        parent::handleQuery();

        switch ($this->_RequestType) {
            case "POST": {
                    $this->handlePost();
                }
                break;
            case "GET":

                $this->handleGet();

                break;

            case "PUT":

                break;

            case "DELETE":

                break;
            default:

                $this->sentResponse(404);

                break;
        }
    }

}
