<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

require_once("AbstractModel.php");
include_once 'Sensor.php';
include_once 'Actuator.php';

/**
 * Description of Device
 *
 * @author omkarkhedekar
 */
/*
  DROP TABLE IF exists `device`;

  CREATE TABLE `device` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_key` varchar(128) NOT NULL,
  `device_name` varchar(64) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `device_key` (`device_key`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf32;

 */

class Device extends AbstractModel {

    //put your code here

    private $onOF = FALSE;
    public $device_id;
    public $user_id;
    public $device_key;
    public $device_name;
    public $is_active;

    //iVars




    protected function getVisiblecolumnList() {
        parent::getVisiblecolumnList();

        return array('user_id', 'device_id', 'device_name', 'is_active', 'device_key', 'updated_on');
    }
    
    protected function getCompactColumnList() {
        

        return array('device_id', 'device_name', 'is_active');
    }

    protected function tableName() {
        parent::tableName();
        return 'device';
    }

    private function addNewDevice(Device $device) {
        $data = Array();
        $data['user_id'] = $device->user_id;
        $data['device_key'] = $device->device_key;
        $data['device_name'] = $device->device_name;

        $id = $this->_DB->insert($this->tableName(), $data);


        if ($id) {
            $resp = array();
            $resp ['device_id'] = $id;
            $resp['device_key'] = $device->device_key;
            $resp ['message'] = 'Device is registered.\r\nUse device key as auth in authorization header';

            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();

            $this->sentResponse(406, $resp);
        }
    }

    private function updateDevice($data, $updateUsing) {
        $colName = $updateUsing['col'];
        $colVal = $updateUsing['val'];
        $this->_DB->where($colName, $colVal);

        $count = $this->_DB->update($this->tableName(), $data);


        if ($count > 0) {
            $resp = array();
            $resp['Coloum'] = $colName;
            $resp['Value'] = $colVal;
            $resp ['message'] = "Number of devices updated " . $count;

            $this->sentResponse(200, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();

            $this->sentResponse(406, $resp);
        }
    }

    private function handleGet() {

//        

        $_getAll = FALSE;
        if (array_key_exists('all_data', $this->_requestData)) {
            $_getAll = ($this->_requestData['all_data'] >= 1);
        }

        if ($this->_RequestPath == "devices") {

            $_userID = -1;
            if (array_key_exists('user', $this->_requestData)) {
                $_userID = $this->_requestData['user'];

                $this->_DB->where('user_id', $_userID);
                $results = array();
                $devicesRS = $this->_DB->get($this->tableName(), NULL, $this->getVisiblecolumnList());
                if ($_getAll) {

                    $sensor = new Sensor();
                    $actuator = new Actuator();

                    foreach ($devicesRS as $_device) {

                        $_deviceID = $_device['device_id'];
                        $sensors = $sensor->getSensorsWithDeviceID($_deviceID, FALSE);
                        $actuators = $actuator->getActuatorsWithDeviceID($_deviceID, FALSE);
                        $_device["sensors"] = $sensors;
                        $_device["actuators"] = $actuators;
                        $results[] = $_device;
                    }
                } else {
                    $results = $devicesRS;
                }
                if (empty($results)) {

                    $this->sentResponse(204);
                } else {
                    $this->sentResponse(200, $results, 'devices');
                }
            } else {

                $this->sentResponse(404);
            }
        } elseif ($this->_RequestPath == "device") {

            $_deviceID = $this->_requestData['device_id'];

            $this->_DB->where('device_id', $_deviceID);
            $results = $this->_DB->getOne($this->tableName(), $this->getCompactColumnList());
            $sensor = new Sensor();
            $actuator = new Actuator();
            $sensors = $sensor->getSensorsWithDeviceID($_deviceID, FALSE);
            $actuators = $actuator->getActuatorsWithDeviceID($_deviceID, FALSE);
            $results["sensors"] = $sensors;
            $results["actuators"] = $actuators;
            

            if (empty($results)) {

                $this->sentResponse(204);
            } else {
                $this->sentResponse(200, $results, 'details');
            }
        } else {
            $this->sentResponse(404);
        }
    }

    private function register() {
        $device = new Device();
        $device->user_id = $this->_requestData['user_id'];
        $device->device_name = $this->_requestData['device_name'];
        $device->device_key = uniqid('SMP_');
        $device->addNewDevice($device);
    }

    private function update() {

//        var_dump($this->_requestData);
//        exit();

        if (isset($this->_requestData['device_id']) || isset($this->_requestData['device_key'])) {

            $data = Array();
            $updateUsing = array();
            $shouldUpdate = FALSE;
            //add device Id if available 
            if (isset($this->_requestData['device_id'])) {
                $updateUsing['col'] = 'device_id';
                $updateUsing['val'] = $this->_requestData['device_id'];
            }

            if (isset($this->_requestData['device_key'])) {
                $updateUsing['col'] = 'device_key';
                $updateUsing['val'] = $this->_requestData['device_key'];
            }

            if (isset($this->_requestData['device_name'])) {
                $data['device_name'] = $this->_requestData['device_name'];
                $shouldUpdate = TRUE;
            }

            if (isset($this->_requestData['is_active'])) {

                $data['is_active'] = $this->_requestData['is_active'];
                $shouldUpdate = TRUE;
            }

            if ($shouldUpdate) {
                $this->updateDevice($data, $updateUsing);
            } else {
                $this->sentResponse(406);
            }
        } else {
            $this->sentResponse(406);
        }
    }

    public function getDeviceWithKey($_KEY) {
        $this->_DB->where('device_key', $_KEY);
        $devicesRS = $this->_DB->getOne($this->tableName(), NULL, $this->getVisiblecolumnList());
        $_device = new Device();
        $_device->device_id = $devicesRS["device_id"];
        $_device->user_id = $devicesRS["user_id"];
        $_device->is_active = $devicesRS['is_active'];
        return $_device;
    }

    public function handleQuery() {
        parent::handleQuery();

        switch ($this->_RequestType) {
            case "POST": {
                    switch ($this->_RequestPath) {

                        case "/register":

                            $this->register();
                            break;

                        case "/update":

                            $this->update();
                            break;

                        default:
                            $this->sentResponse(404);
                            break;
                    }
                }
                break;
            case "GET":

                $this->handleGet();
                /*
                  switch ($this->_RequestPath) {
                  case "devices":

                  $this->listAllUsers();
                  break;

                  default:
                  $this->getUser();
                  break;
                  }
                 */

//                $this->showStatus("Chindya GET");
                break;

            case "PUT":

//                $this->showStatus("Chindya PUT");
                break;

            case "DELETE":

//                $this->showStatus("Chindya DELETE");
                break;
            default:
                echo 'default';
                $this->sentResponse(404);

                break;
        }
    }

}

/*

 */
