<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

$utilsDir = dirname(dirname(__FILE__)) . '/Utils';
include_once $utilsDir . '/MySQLi-Database-Class/MysqliDb.php';
include_once $utilsDir . '/LoggingUtils/OK_Logger.php';

use Mysqlidb as Mysqlidb;
use OK_Logger as OKLogger;

/**
 * Description of AbstractModel
 *
 * @author omkarkhedekar
 */
class AbstractModel {

    //put your code here
    //Private Vars 

    private static $DB_Initialised = FALSE;
    private $_responseCode;
    protected $_DB;
    protected $_RequestType = NULL;
    protected $_requestData = NULL;
    protected $_RequestPath = NULL;
    protected $_RequestHeaders = NULL;

    // constructor
    /*
      $db_config = array(
      'server'   => 'localhost', // Usually localhost
      'database' => 'iotdb', // The database name
      'username' => 'root', // The database username
      'password' => 'root', // The database password
      'verbose' => true // If true errors will be shown
      );
     */

    public function __construct() {

//        if (!AbstractModel::$DB_Initialised) {
        //smilinghome
        //6X!GLMnZ-^-*
        $this->_DB = new Mysqlidb('smilinghome', 'smilinghome', '6X!GLMnZ-^-*', 'iotdb');
        if (!$this->_DB) {
            $error = array();
            $error['Error'] = 'Unable to connect with Database. Please contact Admin';
            $this->sentResponse(503, $error);
            exit();
        } //else {
//                AbstractModel::$DB_Initialised = TRUE;
//            }
//        }
        $this->_RequestType = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
//        $this->_RequestPath =  strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
        //parse_url($url, PHP_URL_PATH)
        $path = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL), PHP_URL_PATH);
        $pathFragments = explode('/', $path);
        $this->_RequestPath = end($pathFragments);
        
        $this->_RequestHeaders = apache_request_headers();
        


        /*
          $path = parse_url($url, PHP_URL_PATH);
          $pathFragments = explode('/', $path);
          $end = end($pathFragments);
         */
        $this->mapInput();
    }

    ///This is for DB 
    //SUB MUST OVERRIDE THIS 
    protected function getVisiblecolumnList() {
        
    }

    protected function tableName() {
        
    }

    private function modelFromDBResult($DBResultSet) {
        
    }

    ////This is for API
    //Private Funcs
    private function get_status_message() {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            567 => 'Failed');
        return ($status[$this->_responseCode]) ? $status[$this->_responseCode] : $status[500];
    }

    private function set_headers() {
        header("HTTP/1.1 " . $this->_responseCode . " " . $this->get_status_message());
        header('Content-Type: application/json');
    }

    private function cleanInputs($data) {

        $clean_input = array();
//        var_dump($data);
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->cleanInputs($v);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $clean_input = trim($data);
        }
        return $clean_input;
    }

    //Now get public ;)
    public function setResponseCode($responseCode) {

        $this->_responseCode = $responseCode;
    }

    public function sentResponse($statusCode, $responseData = NULL, $dataKey = NULL) {
        $this->setResponseCode((($statusCode) ? $statusCode : 200));
        $this->set_headers();
        $_data = array();
        if ($responseData) {
            $key = 'data';
            if ($dataKey && is_string($dataKey)) {

                $key = $dataKey;
            }

            $_data[$key] = $responseData;
        }
        $_data ['status'] = $statusCode;
        echo json_encode($_data);
        return;
    }

    public function mapInput() {

        switch ($this->_RequestType) {
            case "POST": {
                    if (isset($this->_RequestHeaders["Content-Type"]) && $this->_RequestHeaders["Content-Type"] === "application/json") {
                        $inputObj = file_get_contents('php://input');
//                        var_dump($inputObj);
                        $json = json_decode($inputObj);
                        $this->_requestData = $this->cleanInputs($json);
                    } else {
                        $this->_requestData = $this->cleanInputs($_POST);
                    }
                }
                break;
            case "GET":

            case "DELETE":
//                var_dump($this->cleanInputs($_GET));
                $this->_requestData = $this->cleanInputs($_GET);
                break;
            case "PUT":
                $PUT_Req = array();
                parse_str(file_get_contents("php://input"), $PUT_Req);

                $this->_requestData = $this->cleanInputs($PUT_Req);

                break;
            default:
                echo 'is this shitting here ';
                $this->sentResponse(406);
                break;
        }
    }

    protected function handlePost() {
        
    }

    public function handleQuery() {
        
    }

}
