<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

require_once("AbstractModel.php");
/**
 * Description of Actuator
 *
 * @author omkarkhedekar
 */

/**
  CREATE TABLE `actuator` (
  `actuator_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `actuator_name` varchar(45) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `actual_value` int(11) DEFAULT '1',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`actuator_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf32;
 * 
 * 
  INSERT INTO `iotdb`.`actuator`
  (`actuator_id`,
  `device_id`,
  `actuator_name`,
  `is_active`,
  `actual_value`,
  `created_on`,
  `updated_on`)
  VALUES
  (<{actuator_id: }>,
  <{device_id: }>,
  <{actuator_name: }>,
  <{is_active: 1}>,
  <{actual_value: 1}>,
  <{created_on: }>,
  <{updated_on: CURRENT_TIMESTAMP}>);

 */
class Actuator extends AbstractModel {

    //put your code here
    private $actuator_id;
    private $device_id;
    public $actuator_name;
    public $is_active;
    public $actual_value;
    public $created_on;
    public $updated_on;

    protected function getVisiblecolumnList() {
        parent::getVisiblecolumnList();

        return array('actuator_id', 'device_id', 'actuator_name', 'is_active', 'actual_value', 'created_on', 'updated_on');
    }

    protected function tableName() {
        parent::tableName();
        return 'actuator';
    }

    private function addNewActuator(Actuator $device) {

//        var_dump($device);
//        exit();

        $data = Array();
        $data['device_id'] = $device->device_id;
        $data['actuator_name'] = $device->actuator_name;
//        $data['actual_value'] = $device->actual_value;

        $id = $this->_DB->insert($this->tableName(), $data);


        if ($id) {
            $resp = array();
            $resp ['actuator_id'] = $id;
            $resp['actuator_name'] = $device->actuator_name;
            $resp ['message'] = 'Actuator is registered.';

            $this->sentResponse(201, $resp);
        } else {
            $resp = array();
            $resp ['DB_ERROR'] = $this->_DB->lastError();

            $this->sentResponse(406, $resp);
        }
    }

    private function updateActuator($_actuator_id, $updateFields, $sendJSON = TRUE) {
        $this->_DB->where('actuator_id', $_actuator_id);
        if ($this->_DB->update($this->tableName(), $updateFields)) {

            if ($sendJSON) {
                $resp = array();
                $resp ['actuator_id'] = $_actuator_id;
                $resp ['message'] = 'Record is updated.';
                $this->sentResponse(200, $resp);
            } else {
                return TRUE;
            }
        } else {
            if ($sendJSON) {
                $resp = array();
                $resp ['actuator_id'] = $_actuator_id;
                $resp ['message'] = 'Unable to update.';
                $this->sentResponse(567, $resp);
            } else {
                return FALSE;
            }
        }
    }

    private function register() {
        $device = new Actuator();
        $device->device_id = $this->_requestData['device_id'];
        $device->actuator_name = $this->_requestData['actuator_name'];
        $device->actual_value = $this->_requestData['actual_value'];
        $device->addNewActuator($device);
    }

    private function updateRec() {
        $updateFields = array();

        $fromIOT = FALSE;
        if (!array_key_exists('from_iot', $this->_requestData)) {
            $fromIOT = TRUE;
        }


        if (!array_key_exists('actuator_id', $this->_requestData)) {
            $this->sentResponse(204);
            return;
        } else {
            /*
              $data = Array (
              'firstName' => 'Bobby',
              'lastName' => 'Tables',
              'editCount' => $db->inc(2),
              'active' => $db->not());
             */
            $_device_id = $this->_requestData['actuator_id'];
            if (array_key_exists('device_id', $this->_requestData)) {
                $updateFields['device_id'] = $this->_requestData['device_id'];
            }
            if (array_key_exists('actuator_name', $this->_requestData)) {
                $updateFields['actuator_name'] = $this->_requestData['actuator_name'];
            }
            if (array_key_exists('is_active', $this->_requestData)) {
                $updateFields['is_active'] = $this->_requestData['is_active'];
            }
            if (array_key_exists('actual_value', $this->_requestData)) {
                $updateFields['actual_value'] = $this->_requestData['actual_value'];
            }

            $this->updateActuator($_device_id, $updateFields);
        }
    }

    private function handleGet() {

        if (array_key_exists('device_id', $this->_requestData)) {
            $_deviceID = $this->_requestData['device_id'];
            $this->getActuatorsWithDeviceID($_deviceID);
        } elseif (array_key_exists('actuator_id', $this->_requestData)) {
            $_actuatorID = $this->_requestData['actuator_id'];
            $this->getActuatorWithID($_actuatorID);
        } else {
            $this->sentResponse(404);
        }
    }

    protected function handlePost() {
        switch ($this->_RequestPath) {

            case "register":

                $this->register();
                break;

            case "update":

                $this->updateRec();
                break;

            default:
                $this->sentResponse(404);
                break;
        }
    }

    public function getActuatorsWithDeviceID($_deviceID, $JSON = TRUE) {

        $this->_DB->where('device_id', $_deviceID);
        $results = $this->_DB->get($this->tableName(), NULL, $this->getVisiblecolumnList());

        if (empty($results)) {
            if ($JSON) {
                $this->sentResponse(204);
            } else {
                return $results;
            }
        } else {
            if ($JSON) {
                $this->sentResponse(200, $results, 'actuator');
            } else {
                return $results;
            }
        }
    }

    public function getActuatorWithID($_actuatorID, $JSON = TRUE) {

        $this->_DB->where('actuator_id', $_actuatorID);
        $results = $this->_DB->getOne($this->tableName(), NULL, $this->getVisiblecolumnList());

        if (empty($results)) {
            if ($JSON) {
                $this->sentResponse(204);
            } else {
                return $results;
            }
        } else {
            if ($JSON) {
                $this->sentResponse(200, $results, 'actuator');
            } else {
                return $results;
            }
        }
    }

    public function handleQuery() {
        parent::handleQuery();

        switch ($this->_RequestType) {
            case "POST":
                $this->handlePost();
                break;
            case "GET":

                $this->handleGet();

                break;

            case "PUT":
//                $respTest = array();
//                $respTest['Headers'] = $this->_RequestHeaders;
//                $respTest['Path'] = $this->_RequestPath;
//                $respTest['Data'] = $this->_requestData;
//                $this->sentResponse(200, $respTest);
                $this->updateRec();
                break;

            case "DELETE":

//                $this->showStatus("Chindya DELETE");
                break;
            default:

                $this->sentResponse(404);

                break;
        }
    }

}
