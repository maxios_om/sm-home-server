<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Models;

require_once("AbstractModel.php");
require_once("SensorData.php");
require_once("Device.php");
require_once("Sensor.php");
//Sensor::

/**
 * Description of IOTSensor
 *
 * @author omkarkhedekar
 */
class IOTSensor extends AbstractModel {

    //put your code here


    public function record($_device_key) {
        $device = new Device();
        $device = $device->getDeviceWithKey($_device_key);
//        var_dump($device);
//        $this->sentResponse(200, $this->_requestData);
//        exit();
        if ($device->device_id > 0) {
            $sensorData = new SensorData();
            $sensorData->sensor_id = $this->_requestData['sensor_id'];
            $sensorData->state = $this->_requestData['sensor_state'];
            $sensorData->data = $this->_requestData['sensor_data'];
            $sensorData->data_type = $this->_requestData['sensor_data_type'];
            $sensorData->store();
        } else {
            $this->sentResponse(401);
        }
    }

    /*
      public function handleQuery() {
      parent::handleQuery();

      switch ($this->_RequestType) {

      case "GET":
      //                var_dump($this->_requestData);
      $this->record();

      break;
      default:

      $this->sentResponse(404);

      break;
      }
      }
     * 
     */
}
