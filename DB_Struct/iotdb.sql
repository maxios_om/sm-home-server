-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2015 at 04:56 AM
-- Server version: 5.5.38-log
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iotdb`
--
CREATE DATABASE IF NOT EXISTS `iotdb` DEFAULT CHARACTER SET utf32 COLLATE utf32_general_ci;
USE `iotdb`;

-- --------------------------------------------------------

--
-- Table structure for table `actuator`
--
-- Creation: Nov 30, 2014 at 07:54 PM
--

DROP TABLE IF EXISTS `actuator`;
CREATE TABLE IF NOT EXISTS `actuator` (
`actuator_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `actuator_name` varchar(45) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `actual_value` int(11) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32;

--
-- Triggers `actuator`
--
DROP TRIGGER IF EXISTS `on_new_actuator`;
DELIMITER //
CREATE TRIGGER `on_new_actuator` BEFORE INSERT ON `actuator`
 FOR EACH ROW SET NEW.created_on = NOW()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `device`
--
-- Creation: Nov 30, 2014 at 06:11 PM
--

DROP TABLE IF EXISTS `device`;
CREATE TABLE IF NOT EXISTS `device` (
`device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_key` varchar(128) NOT NULL,
  `device_name` varchar(64) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32;

--
-- Triggers `device`
--
DROP TRIGGER IF EXISTS `on_new_device`;
DELIMITER //
CREATE TRIGGER `on_new_device` BEFORE INSERT ON `device`
 FOR EACH ROW SET NEW.created_on = NOW()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sensor`
--
-- Creation: Dec 13, 2014 at 12:57 PM
--

DROP TABLE IF EXISTS `sensor`;
CREATE TABLE IF NOT EXISTS `sensor` (
`sensor_id` int(11) NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `sensor_name` varchar(45) NOT NULL,
  `location` varchar(128) DEFAULT 'Not Available',
  `is_active` int(11) DEFAULT '1',
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32;

--
-- Triggers `sensor`
--
DROP TRIGGER IF EXISTS `on_new_sensor`;
DELIMITER //
CREATE TRIGGER `on_new_sensor` BEFORE INSERT ON `sensor`
 FOR EACH ROW SET NEW.created_on = NOW()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sensor_data`
--
-- Creation: Dec 15, 2014 at 08:42 PM
--

DROP TABLE IF EXISTS `sensor_data`;
CREATE TABLE IF NOT EXISTS `sensor_data` (
`sensor_data_id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `data` float DEFAULT NULL,
  `data_type` varchar(32) DEFAULT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf32;

--
-- Triggers `sensor_data`
--
DROP TRIGGER IF EXISTS `on_new_data`;
DELIMITER //
CREATE TRIGGER `on_new_data` BEFORE INSERT ON `sensor_data`
 FOR EACH ROW SET NEW.created_on = UNIX_TIMESTAMP()
//
DELIMITER ;
DROP TRIGGER IF EXISTS `on_update_zzz`;
DELIMITER //
CREATE TRIGGER `on_update_zzz` BEFORE UPDATE ON `sensor_data`
 FOR EACH ROW SET NEW.updated_on = UNIX_TIMESTAMP(NOW())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--
-- Creation: Nov 29, 2014 at 08:13 AM
--

DROP TABLE IF EXISTS `user_master`;
CREATE TABLE IF NOT EXISTS `user_master` (
`user_id` int(11) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_pass` varchar(128) NOT NULL,
  `nick_name` varchar(64) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `pass_salt` varchar(128) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32;

--
-- Triggers `user_master`
--
DROP TRIGGER IF EXISTS `On_New_User`;
DELIMITER //
CREATE TRIGGER `On_New_User` BEFORE INSERT ON `user_master`
 FOR EACH ROW SET NEW.created_on = NOW()
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--
-- Creation: Dec 13, 2014 at 04:42 PM
--

DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
`user_session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_key` varchar(256) NOT NULL,
  `user-agent` varchar(128) NOT NULL,
  `created_on` int(11) NOT NULL,
  `isValid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf32;

--
-- Triggers `user_session`
--
DROP TRIGGER IF EXISTS `on_new_session`;
DELIMITER //
CREATE TRIGGER `on_new_session` BEFORE INSERT ON `user_session`
 FOR EACH ROW SET NEW.created_on = UNIX_TIMESTAMP()
//
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actuator`
--
ALTER TABLE `actuator`
 ADD PRIMARY KEY (`actuator_id`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
 ADD PRIMARY KEY (`device_id`), ADD UNIQUE KEY `device_key` (`device_key`);

--
-- Indexes for table `sensor`
--
ALTER TABLE `sensor`
 ADD PRIMARY KEY (`sensor_id`);

--
-- Indexes for table `sensor_data`
--
ALTER TABLE `sensor_data`
 ADD PRIMARY KEY (`sensor_data_id`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `user_email` (`user_email`), ADD UNIQUE KEY `pass_salt` (`pass_salt`);

--
-- Indexes for table `user_session`
--
ALTER TABLE `user_session`
 ADD PRIMARY KEY (`user_session_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actuator`
--
ALTER TABLE `actuator`
MODIFY `actuator_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sensor`
--
ALTER TABLE `sensor`
MODIFY `sensor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sensor_data`
--
ALTER TABLE `sensor_data`
MODIFY `sensor_data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_session`
--
ALTER TABLE `user_session`
MODIFY `user_session_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
