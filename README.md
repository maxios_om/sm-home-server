
## Smiling Home (home automation and stats)
> Smiling Home Backend. 

This is backend of POC done for demonstrating thermal stats gathering and basic intraction with controlls like light. 
Represents second layer i.e. Backend

Currently exposes web services for 
- [x] CRUD management for User and IOT devices.
- [x] Record and realy sensor stats.
- [x] Record and realy actuator state.
- [x] Works as rally point for Raspberri PI based in field gateway and mobile devices.

## Terms
    - Sensors: Devices which gather useful information like thermo stats 
    - Actuators: Devices that can control a mechanism or system like room light

## Notes:
This is an old project done for very basic demonstration purpose long time back so some parts are not active at this time.

## Meta

Omkar – [@MAD_Omkar](https://twitter.com/MAD_OmkAR)

[ARomkAR](https://github.com/ARomkAR)